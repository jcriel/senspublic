steal('jquery',function($){

    $.showErrorMessages = function(jqXHR, exception, thrownError,warningMsgPanelSelector,errorMsgPrefixLine) {
		var errorMsg = "";
            if (jqXHR.status === 0) {
                errorMsg = 'Not connect.<br> Verify Network';
            } else if (jqXHR.status == 404) {
                errorMsg = 'Requested page not found. [404]';
            } else if (jqXHR.status == 500) {
                errorMsg = 'Internal Server Error [500] (<a href="#" >See more</a>)<div id="error_report" style="display:none;">'+jqXHR.responseText+'</div>';
            } else if (exception === 'parsererror') {
                errorMsg = 'Requested JSON parse failed.';
            } else if (exception === 'timeout') {
                errorMsg = 'Time out error.';
            } else if (exception === 'abort') {
                errorMsg = 'Ajax request aborted.';
            } else {
                errorMsg = jqXHR.statusText+'  ('+jqXHR.status+')';
            }
        if($(warningMsgPanelSelector)){    
        	$(warningMsgPanelSelector).html(errorMsgPrefixLine+". <br>Please retry later. If this keep occuring contact the administrator<br> Error details: "+errorMsg).show();   
        }else{
        	console.warn("error occured but no valid warnig msg elemend selected:" + warningMsgPanelSelector)
        }
       };

    
})